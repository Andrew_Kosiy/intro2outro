const express = require('express');
const s3 = require('../aws/s3');
const router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  s3.getVideo(req.query.scenario, req.query.name, function (data) {
    if (!data) {
      res.status(404);
      res.send('File not found');

      return;
    }

    res.contentType(data.ContentType);
    res.send(data.Body);
  });
});

module.exports = router;
