const AWS = require('aws-sdk');
const config = require('../config.json');
const accessKeyId = config.accessKeyId;
const secretAccessKey = config.secretAccessKey;

const bucketName = 'intro2outrobucket';

AWS.config.update({
  credentials: {
    accessKeyId: accessKeyId,
    secretAccessKey: secretAccessKey
  }
});

s3 = new AWS.S3({
  apiVersion: '2006-03-01',
  params: { Bucket: bucketName }
});

function getVideo(scenario, name, callback) {
  s3.getObject({
    Bucket: bucketName,
    Key: `Scenario_${scenario}/${name}`
  }, function (err, data) {
    if (err) {
      callback();
    } else {
      callback(data);
    }
  });
}

module.exports = {
  getVideo: getVideo
};
